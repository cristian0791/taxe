<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        'name', 'email', 'password',
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /* Relations */
    public function import(){
        return $this->hasMany('App\Models\UserUploads', 'user_id', 'id');
    }
    
    public function utilizatori(){
        return $this->hasMany('App\Models\Utilizatori', 'user_id', 'id');
    }
    /* end relations */
    
    
    public function createImport($data){
        $import = $this->import()->create([
            'user_id'   => $this->id,
            'filename'  => $data['filename'],
            'original_filename' => $data['original_filename'],
            'active'    => 1
        ]);
    }
    
    public function hasFile(){
        return $this->getImport();
    }
    
    public function getImport(){
        return $this->import()->where('active', 1)->first();
    }
    
    public function disableImport($count = 0){
        return $this->import()->where('active', 1)->update(['active' => 0, 'count' => $count]);
    }
    
    public function deleteImport(){
        return $this->import()->where('active', 1)->first()->delete();
    }
    
    public function importHistory(){
        return $this->import()->where('active', 0)->get();
    }
    
    public function rollbackImport($importId){
        $this->import()->where('id', $importId)->first()->delete();
        return $this->utilizatori()->where('import_id', $importId)->delete();
    }
    
}
