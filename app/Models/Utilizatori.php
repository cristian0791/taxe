<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Utilizatori extends Model{
    protected $table = "utilizatori";
    
    protected $guarded = [''];
    
    protected $except = ['id','created_at', 'updated_at', 'user_id', 'import_id'];
    
    public function columns(){
        $schema = \Schema::getColumnListing($this->table);
        $columns = [];
        
        foreach($schema as $column) {
            in_array($column, $this->except) ? '' : $columns[] = $column; 
        }
        
        return $columns;
    }
}
