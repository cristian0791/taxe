<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserUploads extends Model{
    protected $table = "users_uploads";
    
    protected $guarded = [''];
}
