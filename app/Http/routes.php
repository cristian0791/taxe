<?php

Route::group(['middleware' => 'web'], function () {
    
    Route::group(['namespace' => 'App', 'middleware' => 'auth'], function () {
        Route::group(['namespace' => 'Import'], function () {
            Route::get('upload', ['as' => 'app.import', 'uses' => 'Upload@index']);
            Route::post('upload', ['as' => 'app.import.upload', 'uses' => 'Upload@upload']);
            
            Route::get('process', ['as' => 'app.process', 'uses' => 'Process@index']);
            Route::post('process', ['as' => 'app.process.import', 'uses' => 'Process@import']);
            Route::get('process/delete', ['as' => 'app.process.delete', 'uses' => 'Process@deleteFile']);
            Route::get('{importId}/rollback', ['as' => 'app.process.rollback', 'uses' => 'Process@rollback'])->where('importId', '[0-9]+');
        });
    });

    Route::group(['namespace' => 'Auth'], function () {
        Route::group(['middleware' => 'guest'], function () {
            Route::get('login', ['as' => 'auth.login', 'uses' => 'Authentication@index']);
            Route::post('login', ['as' => 'auth.login', 'uses' => 'Authentication@login']);
        });
        
        Route::get('logout',['as' => 'auth.logout', 'uses' => 'Authentication@logout']);
    });
    
});
