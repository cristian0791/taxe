<?php
namespace App\Http\Controllers\App\Import;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Base\App;

class Upload extends App {

    public function index(){
        if($this->user->hasFile()){
            return redirect()->route('app.process');
        }
        
        return view('app.import.upload',[
            'importHistory'     => $this->user->importHistory()
        ]);
    }
    
    public function upload(Request $request){
        $errors = $this->validator($request);
        
        if(count($errors) === 0) {
            $file = $request->file('file');
            $originalFilename = $file->getClientOriginalName();
            $filename = rand(11111, 99999).'.'.$file->getClientOriginalExtension();
            
            if($file->move($this->importPath, $filename)){
                $this->user->createImport([
                    'filename' => $filename,
                    'original_filename' => $originalFilename,
                ]);
            }
            
            $this->response->setResponse(['redirect_to' => ""]);
            $this->response->setSuccess(TRUE);
        }
        
        $this->response->setErrors($errors);
        return $this->response->toJson();
    }
    
    protected function validator($request){
        $validator = Validator::make($request->all(),[]);
        
        $errors = $validator->messages();
        
        if (!$request->hasFile('file')) {
            $errors->add('file', 'Te rugam sa alegi un fisier pentru incarcare');
            return $errors;
        }
         
        $file = $request->file('file');
        
        if($file->getClientOriginalExtension() != 'csv') {
            $errors->add('file', 'Fisierul trebuie sa fie tip csv');
            return $errors;
        }
        
        if($file->getClientMimeType() != 'text/csv') {
            $errors->add('file', 'Fisierul trebuie sa fie tip csv');
            return $errors;
        }
    }
    
}
