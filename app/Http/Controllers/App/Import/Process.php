<?php
namespace App\Http\Controllers\App\Import;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Base\App;
use File;
use App\Libraries\Csv;

class Process extends App{
    
    public function index(){
        $import = $this->user->getImport();
        
        if(!$import) {
            return redirect()->route('app.import');
        }
        
        $destination = $this->importPath . '/' . $import->filename;
        $csv = new Csv($destination);
        $csv = $csv->preview();
        
        return view('app.import.process',[
            'import'    => $import,
            'data'      => $csv['data'],
            'info'      => $csv['info'],
            'columns'   => $csv['columns']
        ]);
    }
    
    public function import(Request $request){
        $errors = $this->validator($request); 
        
        if(count($errors) === 0 ) {
            $import = $this->user->getImport();
            
            $file = $this->importPath . '/' . $import->filename;
            $csv = new Csv($file);
            $csv->import_id = $import->id;
            
            $count = $csv->insert($request->thead);
            
            if($count){
                if($this->user->disableImport($count)){
                    if (File::isFile($file)){
                        File::delete($file);
                    }
                }
            }
            
            $this->response->setResponse([
                'redirect_to'   => route('app.import')
            ]);
            $this->response->setSuccess(TRUE);
        }
        
        $this->response->setErrors($errors);
        return $this->response->toJson();
    }
    
    
    public function deleteFile(){
        $import = $this->user->getImport();
        
        if($import){
            if($this->user->deleteimport()) {
                $file = $this->importPath .'/'.$import->filename;
                if (File::isFile($file)){
                    File::delete($file);
                }
            }
        }
        
        return redirect()->route('app.import');
    }
    
    public function rollback(Request $request, $importId){
        $this->user->rollbackImport($importId);
        
        return redirect()->route('app.import');
    }
    
    
    protected function validator($request){
        $validator = Validator::make($request->all(),[]);
        $errors = $validator->messages();

        $sameValues = array_filter( array_unique( array_diff_assoc( $request->thead, array_unique( $request->thead ) ) ) );
        
        if(count($sameValues) > 0) {
            foreach($sameValues as $sameValue) {
                $errors->add('thead', 'Poate exista o singura coloana de tipul "'.$sameValue.'"');
            }
            
            return $errors;
        }
        
        if(!in_array('nume',$request->thead)) {
            $errors->add('thead', 'Te rugam alege coloana "nume" ');
            return $errors;
        }
        
        if(!in_array('prenume',$request->thead)) {
            $errors->add('thead', 'Te rugam alege coloana "prenume" ');
            return $errors;
        }
    }
    
}
