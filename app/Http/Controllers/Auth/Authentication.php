<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Base\Site;
use Auth;

class Authentication extends Site
{
    
    public function index(){
        return view('auth.login');
    }
    
    public function login(Request $request){
        $errors = $this->validator($request->all());
        
        if(count($errors) === 0) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'active' => 1], $request->remember)) {
                $this->response->setResponse([
                    'redirect_to'   => route('app.import')
                ]);
                $this->response->setSuccess(TRUE);
            }
        }
        
        $this->response->setErrors($errors);
        return $this->response->toJson();
    }
    
    public function logout(){
        if (Auth::check()) {
            Auth::logout();
            return redirect()->route('auth.login');
        }
        
        return redirect()->route('app.import');
    }
    
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        return $validator->messages();
    }
}


