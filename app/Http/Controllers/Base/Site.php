<?php
namespace App\Http\Controllers\Base;

use App\Http\Controllers\Base\Controller;
use App\Libraries\Response;


class Site extends Controller{
    
    protected $response;
    
    public function __construct(){
        $this->response = new Response();
    }
}
