<?php
namespace App\Http\Controllers\Base;

use App\Http\Controllers\Base\Controller;
use App\Libraries\Response;

use Auth;

class App extends Controller {
    
    protected $user;
    protected $response;
    protected $importPath;
    
    public function __construct(){
        $this->response = new Response();
        $this->user = Auth::user();
        $this->importPath  = storage_path("app/users/import");
    }
}
