<?php
namespace App\Libraries;
use parseCSV;
use Auth;
use App\Models\Utilizatori;

class Csv {
    
    private $csv;
    private $destination;
    private $chunk = 5000;
    
    public $import_id;
    
    public function __construct($destination){
        $this->csv = new parseCSV();
        $this->destination = $destination;
    }
    
    public function __destruct(){
        unset($this->csv);
        unset($this->destination);
        unset($this->import_id);
    }
    
    public function preview($limit = 5){
        $columns = (new Utilizatori)->columns();
        $this->csv->limit = $limit;
        $this->csv->auto($this->destination);
        $data = $this->csv->data;
        
        $toReturn = ['data' => '', 'info' => '', 'columns' => ''];
        
        if(count($data) > 0) {
            $tableHead = array_keys($data[0]);
            $info = [];
            foreach($tableHead as $csvColumn) {
                in_array($csvColumn,$columns) ? $info[$csvColumn] = true : $info[$csvColumn] = false;
            }
            
            $toReturn = ['data' => $data, 'info' => $info, 'columns' => $columns];
        }
        
        return $toReturn;
    }
    
    public function insert($fields){
        $this->csv->auto($this->destination);
        $chunks = array_chunk($this->csv->data, $this->chunk);
        
        $user_id = Auth::user()->id;
        $created_at = $updated_at = (new \DateTime())->format('Y-m-d H:i:s');
  
        foreach($chunks as $csvData) {
            
            $insert = [];
            
            foreach($csvData as $row) {
                $rowdata = array_values($row);
                $temp = [];

                foreach($fields as $key => $field) {

                    if(strlen($field) != 0) {
                        $temp[$field] = $rowdata[$key];
                        $temp['user_id'] = $user_id;
                        $temp['created_at'] = $created_at;
                        $temp['updated_at'] = $updated_at;
                        $temp['import_id'] = $this->import_id;
                    }

                }

                $insert[] = $temp;
            }
            
            Utilizatori::insert($insert);
        }  
        
        return count($this->csv->data);
    }
    
}
