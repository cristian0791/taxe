<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $nowDate = (new \DateTime)->format('Y-m-d H:i:s');
        
        DB::table('users')->insert([
            'firstname'     => 'Nicolae', 
            'lastname'      => 'Olariu',
            'email'         => 'email@test.com',
            'password'      => bcrypt('test'),
            'active'        => 1,
            'created_at'    => $nowDate,
            'updated_at'    => $nowDate
        ]);
    }
}
