$(document).ready(function(){ 
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('.formjs').form();
    
    if($('.deleteImport').is('*')) {
       $('.deleteImport').on('click', function(e){
            return confirm("Esti sigur ca vrei sa stergi toti utilizatorii importati ?");
       });
    }
});
