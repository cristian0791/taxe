@extends('layouts.app')

@section('title')
    Incarca fisier
@endsection

@section('javascript')
    <script src="{{ asset("assets/jquery-file-upload/js/jquery.fileupload.js") }}"></script>
    <script src="{{ asset("assets/jquery-file-upload/js/jquery.iframe-transport.js") }}"></script>
@endsection

@section('content')
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">Incarca fisier</div>
        <div class="panel-body">
            {!! Form::open(['route' => 'app.import.upload', 'method' => 'POST', 'files' => true, 'class' => 'formjs']) !!}
            <div class="col-lg-6 col-lg-offset-4">     
                <div class="form-group file">
                    {{ Form::label('file', 'Incarcare fisier') }}
                    {{ Form::file('file', ['class' => 'single-fileupload btn btn-default btn-file']) }}
                </div>
                
                {{ Form::button('Start incarcare',['type' => 'submit', 'class' => 'btn btn-default submit-btn']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @if(count($importHistory) != 0)
        <table class="table table-condensed table-bordered">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Ora</th>
                    <th>Numar utilizatori importati</th>
                    <th>Nume fisier</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
              @foreach($importHistory as $import)
                    <tr>
                        <td>{{ $import->created_at->format('d-m-Y') }}</td>
                        <td>{{ $import->created_at->format('H:i:s') }}</td>
                        <td>{{ $import->count }}</td>
                        <td>{{ $import->original_filename }}</td>
                        <td class="text-center"><a class="deleteImport" href="{{ route('app.process.rollback',['id' => $import->id]) }}"><i class="fa fa-times"></i></a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
</div>
@endsection