@extends('layouts.app')

@section('title')
    Procesare fisier
@endsection

@section('content')
<div class="col-md-12">
    <div class="row">
        <div class="col-lg-6">
            <p><b> {{ $import->original_filename }} </b></p>
        </div>
    </div>
    <br />
    {{ Form::open(['method' => 'app.process.import', 'class' => 'formjs' ,'method' => 'post']) }}
        <table class="table table-condensed table-bordered">
            <thead>
                <tr>
                    @foreach($info as $field => $set)
                    <th>
                        <select name="thead[]" class="form-control input-sm">
                            <option value=""></option>
                            @foreach($columns as $column)
                                <option {{ $field == $column ? 'selected="selected"' : '' }} value="{{ $column }}">{{ $column }}</option>
                            @endforeach
                        </select>
                    </th>
                    @endforeach
                </tr>
            </thead>  

            <tbody>

                @foreach($data as $row) 

                    <tr>
                        @foreach($info as $field => $set)
                            <td>{{ $row[$field] }}</td>
                        @endforeach
                    </tr>

                @endforeach
                
                 <tr>
                    @foreach($info as $field => $set)
                        <td> ... </td>
                    @endforeach
                </tr>
                
            </tbody>
        </table>
        <ul class="list-inline pull-right ">
            <li><a class="btn btn-danger" href="{{ route('app.process.delete') }}"><i class="fa fa-fw fa-times"></i> Sterge</a></li>
            <li><button class="btn btn-success submit-btn" type="submit"><i class="fa fa-fw fa-upload"></i> Start import</button></li>
        </ul>
        <div class="formjs-custom-message thead"></div>
    {{ Form::close() }}
</div>
@endsection