@extends('layouts.app')

@section('title')
    Login
@endsection

@section('content')
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">Login</div>
        <div class="panel-body">
            <form class="form-horizontal formjs" role="form" method="POST" action="{{ route('auth.login') }}">
                {!! csrf_field() !!}

                <div class="form-group email">
                    <label class="col-md-4 control-label">Adresa de email</label>

                    <div class="col-md-6">
                        <input type="email" value="email@test.com" class="form-control" name="email">
                    </div>
                </div>

                <div class="form-group password">
                    <label class="col-md-4 control-label">Parola</label>

                    <div class="col-md-6">
                        <input type="password" value="test" class="form-control" name="password">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"> Tine minte parola
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-fw fa-sign-in"></i>Login
                        </button>
                    </div>
                </div>

                 <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <p>Test email : email@test.com</p>
                        <p>Parola : test</p>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
